
import bpy


class VIEW3D_PIE_MT_bjstoolbox(bpy.types.Menu):
    # label is displayed at the center of the pie menu.
    bl_label = "BJsToolBox"

    def draw(self, context):
        layout = self.layout
        pie = layout.menu_pie()
        if context.mode == "EDIT_MESH":
            pie.operator("object.quick_set_origin")


def register():
    bpy.utils.register_class(VIEW3D_PIE_MT_bjstoolbox)


def unregister():
    bpy.utils.unregister_class(VIEW3D_PIE_MT_bjstoolbox)
