
import bpy

bl_info = {
    "name": "BJsTools",
    "author": "Bjoern Siegert",
    "version": (0, 0, 1),
    "blender": (2, 80, 0),
    "location": "",
    "description": "Bjoern's custom tools",
    "warning": "",
    "category": "",
}


from . import origin_module
from . import pie_menu


def register():
    origin_module.register()
    pie_menu.register()


def unregister():
    origin_module.unregister()
    pie_menu.register()


if __name__ == "__main__":
    register()