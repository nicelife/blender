"""Bender scripting test to find objects in camera frustum."""

import bpy
from mathutils import Vector
import math
print('='*10)
P = bpy.data.objects['Empty']
print('P.location:', P.location)
cube = bpy.data.objects['Cube']
bounds_world = [cube.matrix_world @ Vector(corner) for corner in cube.bound_box]
#print(bounds_world)

cam_ob = bpy.context.scene.camera
cam_viewframe = bpy.data.cameras[cam_ob.name].view_frame()
M = cam_ob.matrix_world.copy()
print('camera M:\n', M)
M.invert()
print('camera inverse M:\n', M)

bounds_camera = [M @ corner for corner in bounds_world]
#print(bounds_camera)

P_camera = M @ P.location
print('P_camera:', P_camera) 
print('P_camera.z:', P_camera.z)

canvas_distance = cam_viewframe[0].z
points_image_plane = [
    Vector(
    ((point.x / -point.z) * canvas_distance, 
     (point.y / -point.z) * canvas_distance, 
      0)
     ) 
    for point in bounds_camera
]
#print(points_image_plane, '\n-------')

P_canvas = Vector((
    (P_camera.x / P_camera.z) * canvas_distance,
    (P_camera.y / P_camera.z) * canvas_distance,
    0
))
print('point image plane: ', P_canvas)

# 

resolution_x = 1024.
resolution_y = 1024.

aspect = resolution_y/resolution_x

# this is not correct only for testing
w = 2 * cam_viewframe[0].x
h = 2 * cam_viewframe[0].x

points_normalized = [
    Vector(
        (point.x / w,
         point.y / h, 
          0)
    ) 
    for point in points_image_plane
]


P_normalized = Vector(
    ((P_canvas.x + w/2) / w,
     (P_canvas.y + h/2) / h, 
     0)
)

print('P_normalized:', P_normalized)

print('-'*5)

points_pixel = [
      Vector(
        (math.floor(point.x * resolution_x),
         math.floor(point.y * resolution_y), 
         0)
    ) 
    for point in points_normalized  
]

P_pixel = Vector(
    (math.floor(P_normalized.x * resolution_x),
     math.floor(P_normalized.y * resolution_y), 
     0)
)

print('P_pixel:', P_pixel)
